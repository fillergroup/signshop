import "./App.css";
import React from "react";
import Nav from "./components/Nav.js";
import Body from "./components/Body.js";
import Footer from "./components/Footer.js";

function App() {
  return (
    <div className="flex flex-col h-screen">
      {Nav()}
      {Body()}
      {Footer()}
    </div>
  );
}

export default App;

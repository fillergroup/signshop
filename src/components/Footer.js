export default function Footer() {
  return (
    <div>
      <footer className="bg-yellow-100 p-2 text-center bottom-px">
        <div className="text-center text-gray-700">
          ©Copyright:{" "}
          <a className="text-gray-700" href="mailto:hortonwhy@protonmail.com">
            Wyatt Horton
          </a>
        </div>
      </footer>
    </div>
  );
}

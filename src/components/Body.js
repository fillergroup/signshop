import featuredProducts from "./featuredProducts.js";
import React, { useState, useEffect } from "react";

const products = [
  {
    id: 1,
    name: "Basic Tee",
    href: "#",
    imageSrc:
      "https://tailwindui.com/img/ecommerce-images/product-page-01-related-product-01.jpg",
    imageAlt: "Front of men's Basic Tee in black.",
    price: "$35",
    color: "Black",
  },
  {
    id: 2,
    name: "Basic Tee",
    href: "/product/2",
    imageSrc:
      "https://tailwindui.com/img/ecommerce-images/product-page-01-related-product-01.jpg",
    imageAlt: "Front of men's Basic Tee in black.",
    price: "$35",
    color: "Black",
  },
];

export default function Body() {
  const queryString = window.location.href;
  const queryArray = queryString.split("/");
  const pageNumber = queryArray[queryArray.length - 1];
  useEffect(() => {
    console.log(pageNumber);
  }, [pageNumber]);
  if (pageNumber > 0) {
    console.log("greater");
  }
  return <div>{featuredProducts(products)};</div>;
}
